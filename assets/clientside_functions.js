// Define clientside callback functions
//   Need to register them down in the bottom of this file for them to work as clientside functions
//   Doing this to improve reusability of the building blocks - Dash only allows one callback to update
//   figures, so we need a wrapper function to decide which other function to use. Having all the functionality
//   in the same function will be very unreadable and difficult to maintain / extend, so it's better to have
//   them separate and named
function slice_3d(fig, xrange, yrange, zrange){
    new_fig = JSON.parse(JSON.stringify(fig))
    new_fig['layout']['scene']['xaxis']['range'] = xrange
    new_fig['layout']['scene']['yaxis']['range'] = yrange
    new_fig['layout']['scene']['zaxis']['range'] = zrange
    return new_fig
}

function set_pt_sz(ptsz, figs){
    for (trace in figs[0]['data']){ 
        if (figs[0]['data'][trace]['name']!='Blast Boundary') {
            figs[0]['data'][trace]['marker']['size'] = ptsz 
        }
    }
    for (trace in figs[1]['data']){ 
        if (figs[1]['data'][trace]['name']!='Blast Boundary') {
            figs[1]['data'][trace]['marker']['size'] = ptsz 
        }
    }
    for (trace in figs[2]['data']){ 
        if (figs[2]['data'][trace]['name']!='Blast Boundary') {
            figs[2]['data'][trace]['marker']['size'] = ptsz 
        }
    }
    return figs
}

function sync_zoom(relayout, figs) {
    new_layout = JSON.parse(JSON.stringify(relayout))
    // Set all layouts to be like the changed one
    figs[0]['layout']['xaxis']['range']=[new_layout['xaxis.range[0]'],new_layout['xaxis.range[1]']]
    figs[0]['layout']['yaxis']['range']=[new_layout['yaxis.range[0]'],new_layout['yaxis.range[1]']]
    figs[1]['layout']['xaxis']['range']=[new_layout['xaxis.range[0]'],new_layout['xaxis.range[1]']]
    figs[1]['layout']['yaxis']['range']=[new_layout['yaxis.range[0]'],new_layout['yaxis.range[1]']]
    figs[2]['layout']['xaxis']['range']=[new_layout['xaxis.range[0]'],new_layout['xaxis.range[1]']]
    figs[2]['layout']['yaxis']['range']=[new_layout['yaxis.range[0]'],new_layout['yaxis.range[1]']]
    return figs
}

function select_hole_area(figs, masked, mask) {
    if (masked!='mask') {
        for (f in figs) {
            for (trace in figs[f]['data']){
                if (figs[f]['data'][trace]['name']=='Blast Boundary') { figs[f]['data'][trace]['opacity'] = 0 } 
                else { figs[f]['data'][trace]['selecteddata'] = []}
            }
        }
    } else {
        for (f in figs) {
            for (trace in figs[f]['data']){
                if (figs[f]['data'][trace]['name']=='Blast Boundary') { figs[f]['data'][trace]['opacity'] = 1 } 
                else { figs[f]['data'][trace]['selectedpoints'] = mask[f][trace] }
            }
        }
    }
    return figs
}

function clientside_function_wrapper_2d(relayout_eng, relayout_geo, relayout_mwd, ptsz, mask, data_eng, data_geo, data_mwd, masked_eng_ass, masked_geo, masked_mwd){
    triggered = window.dash_clientside.callback_context.triggered.map(t=>t['prop_id'])
    if (triggered.length==0) { return window.dash_clientside.no_update; } // this looks like a page load, no blast loaded yet
    // Too many simultaneous layout changes. Looks like an initial load.
    // Since it's an initial load, print all pictures as created on the server side
    // This initial load could also be empty figures
    if (triggered.length>1) { return [data_eng, data_geo, data_mwd] }
    var r = [ JSON.parse(JSON.stringify(data_eng)), JSON.parse(JSON.stringify(data_geo)), JSON.parse(JSON.stringify(data_mwd)) ]
    if (triggered.includes('viz_eng_ass.relayoutData')) { r = sync_zoom(relayout_eng, r) }
    if (triggered.includes('viz_pred2d_geo.relayoutData')) { r = sync_zoom(relayout_geo, r) }
    if (triggered.includes('viz_pred2d_mwd.relayoutData')) { r = sync_zoom(relayout_mwd, r) }
    r = set_pt_sz(ptsz, r) // pt sz is a state. Always set this one.
    r = select_hole_area(r, mask, [masked_eng_ass, masked_geo, masked_mwd]) // mask is a state. Always set this one.
    return r
}

// Register clientside callback functions
window.dash_clientside = Object.assign(
    {}, 
    window.dash_clientside, {
        clientside: {          
            slice_3d:slice_3d,
            set_pt_sz:set_pt_sz,
            sync_zoom:sync_zoom,
            clientside_function_wrapper_2d:clientside_function_wrapper_2d
        }
    }
);