import dash
import dash_design_kit as ddk
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash.dependencies import ClientsideFunction, Input, Output, State
import uuid
import pandas as pd
import numpy as np

from shapely.geometry import Point, MultiPoint
from plotly.graph_objects import Scattergl
from boundary_utils import get_boundary, within

app = dash.Dash(__name__)
server = app.server

customer = 'spence'
# This will initialise everything that needs to be initialised regarding the cluster access import cluster_setup will do too, if mlflow_client is not needed
from cluster_setup import spark, mlflow_client
import mlflow

local_dir = 'assets/' # Gotta put the artifacts in this folder, or else Flask won't find a route for them
import os
if not os.path.exists(local_dir):
    os.mkdir(local_dir)

metrics = ['mae', 'mse', 'r2', 'rmse']
data_experiment = mlflow.get_experiment_by_name('/Shared/DfO/000 Experiments/Prep Historical Data')
pred_experiment = mlflow.get_experiment_by_name('/Shared/DfO/000 Experiments/Recommendations')

# Get data options
data_runs = mlflow.search_runs(data_experiment.experiment_id, filter_string='tags.Customer="%s" and tags.DfO_ready="True"'%(customer))
data_runs = data_runs[data_runs['status']=='FINISHED']
data_opts = [{'label':r['tags.mlflow.runName'], 'value':r['run_id']} for i,r in data_runs.iterrows()]

pred2d_geo_runs = mlflow.search_runs(pred_experiment.experiment_id, filter_string='tags.Customer="%s" and tags.Design="2D" and tags.Parameters="Burden and Spacing"'%(customer))
pred2d_geo_runs = pred2d_geo_runs[pred2d_geo_runs['status']=='FINISHED']
pred2d_geo_opts = [{'label':r['tags.mlflow.runName'], 'value':r['run_id']} for i,r in pred2d_geo_runs.iterrows()]

pred2d_mwd_runs = mlflow.search_runs(pred_experiment.experiment_id, filter_string='tags.Customer="%s" and tags.Design="2D" and tags.Parameters="Energy Factors"'%(customer))
pred2d_mwd_runs = pred2d_mwd_runs[pred2d_mwd_runs['status']=='FINISHED']
pred2d_mwd_opts = [{'label':r['tags.mlflow.runName']+' '+r['params.Blast'], 'value':r['run_id']} for i,r in pred2d_mwd_runs.iterrows()]

pred3d_runs = mlflow.search_runs(pred_experiment.experiment_id, filter_string='tags.Customer="%s" and tags.Design="3D" and tags.Parameters="Energy Factors"'%(customer))
pred3d_runs = pred3d_runs[pred3d_runs['status']=='FINISHED']
pred3d_opts = [{'label':r['tags.mlflow.runName']+' '+r['params.Blast'], 'value':r['run_id']} for i,r in pred3d_runs.iterrows()]

app.title = 'Design for Outcome - Prediction Viewer (Spence)'

app.layout = ddk.App(
    show_editor=False,
    children=[
        ddk.Card(children=[
            ddk.Row(children=[
                html.Caption('Data', style={'width':'15pc'}, id='a'),
                dcc.Dropdown(id='dd_eng_ass_data', options=data_opts, style={'width':'85pc'}),
            ]),
            ddk.Row(children=[
                html.Caption('Eng. Ass. Column', style={'width':'15pc'}),
                # TODO: fill these in dynamically
                dcc.Dropdown(id='dd_eng_ass_col', options=[{'label':'acf', 'value':'acf'}], style={'width':'85pc'})
            ]),
            ddk.Row(children=[
                html.Caption('Pred. 2D Geo Run', style={'width':'15pc'}),
                dcc.Dropdown(id='dd_pred2d_geo_data', options=pred2d_geo_opts, style={'width':'85pc'})
            ]),
            ddk.Row(children=[
                html.Caption('Pred. 2D MWD Run', style={'width':'15pc'}),
                dcc.Dropdown(id='dd_pred2d_mwd_data', options=pred2d_mwd_opts, style={'width':'85pc'})
            ]),
            ddk.Row(children=[
                html.Caption('Pred. 3D MWD Run', style={'width':'15pc'}),
                dcc.Dropdown(id='dd_pred3d_mwd_data', options=pred3d_opts, style={'width':'85pc'})
            ]),
            html.Button('Load', id='btn_load', n_clicks=0)
        ]),
        ddk.Card(width=33.3, children=[
            ddk.CardHeader(title='Engineer\' Assessment'),
            ddk.Graph(id='viz_eng_ass'),
        ]),
        ddk.Card(width=33.3, children=[
            ddk.CardHeader(title='2D Domain Prediction from Geo Only'),
            ddk.Graph(id='viz_pred2d_geo')
        ]),
        ddk.Card(width=33.3, children=[
            ddk.CardHeader(title='2D Domain Prediction from MWD Only'),
            ddk.Graph(id='viz_pred2d_mwd')
        ]),
        ddk.Card(width=100, children=[
            dcc.Slider(id='2d_pt_sz', min=1, max=15, value=3, step=1.0),
            dcc.Slider(id='dilation_sz', min=0, max=50, value=15, step=1.0),
            dcc.Checklist(id='chk_mask', options=[{'label':'Mask', 'value':'mask'}], value=[])
        ]),
        ddk.Card(width=100, children=[
            ddk.CardHeader(title='3D Prediction from MWD Only'),
            ddk.Block(style={'width':'100pc'},
                children=[
                    html.Caption('Easting'),
                    dcc.RangeSlider(id='sld_3d_x', min=0, max=1000000, value=[0, 1000000], step=1.0)
                ]
            ),
            ddk.Block(style={'width':'100pc'},
                children=[
                    html.Caption('Northing'),
                    dcc.RangeSlider(id='sld_3d_y', min=0, max=1000000, value=[0, 1000000], step=1.0)
                ]
            ),
            ddk.Block(style={'width':'100pc'},
                children=[
                    html.Caption('Elevation'),
                    dcc.RangeSlider(id='sld_3d_z', min=0, max=1000000, value=[0, 1000000], step=1.0)
                ]
            ),
            dcc.Store(id='clientside_3d_fig'),
            dcc.Store(id='clientside_viz_eng_ass'),
            dcc.Store(id='clientside_viz_pred2d_geo'),
            dcc.Store(id='clientside_viz_pred2d_mwd'),
            dcc.Store(id='masked_eng_ass'),
            dcc.Store(id='masked_geo'),
            dcc.Store(id='masked_mwd'),
            ddk.Graph(id='viz_pred3d_mwd')
        ]),
        ddk.Card(width=100, children=[
            ddk.CardHeader(title='Pred. 2D Geo Info'),
            html.Caption('Features used:', style={'width':'150px'}),
            html.Div(id='geo_feats'),
            html.Iframe(id='2d_geo_lbl', style={'width':'30pc', 'height':'400px'}),
            html.Iframe(id='2d_geo_mae', style={'width':'30pc', 'height':'400px'}),
            html.Iframe(id='2d_geo_feat_imp', width=40, height=100, style={'width':'40pc', 'height':'400px'}),
    
            ddk.DataTable(id='2d_geo_metrics', columns=[{'name':x, 'id':x} for x in metrics], style_table={'width':'100pc'}, style_cell={'text-align':'left', 'width':round(100/len(metrics))})
        ]),
        ddk.Card(width=100, children=[
            ddk.CardHeader(title='Pred. 2D MWD Info'),
            html.Iframe(id='2d_mwd_lbl', style={'width':'30pc', 'height':'400px'}),
            html.Iframe(id='2d_mwd_mae', style={'width':'30pc', 'height':'400px'}),
            html.Iframe(id='2d_mwd_feat_imp', style={'width':'40pc', 'height':'400px'}),
            ddk.DataTable(id='2d_mwd_metrics', columns=[{'name':x, 'id':x} for x in metrics], style_table={'width':'100pc'}, style_cell={'text-align':'left', 'width':round(100/len(metrics))})
        ]),
        ddk.Card(width=100, children=[
            ddk.CardHeader(title='Pred. 3D MWD Info'),
            html.Iframe(id='3d_mwd_mae', style={'width':'30pc', 'height':'400px'}),
            html.Iframe(id='3d_mwd_feat_imp', style={'width':'70pc', 'height':'400px'}),
            ddk.DataTable(id='3d_mwd_metrics', columns=[{'name':x, 'id':x} for x in metrics], style_table={'width':'100pc'}, style_cell={'text-align':'left', 'width':round(100/len(metrics))})
        ]),
    ]
)

# NAUGHTY! Don't use global variables! I'm doing this here just for simplicity!
df_BlastHole, df_eng_ass, df_pred2d_geo, df_pred2d_mwd, df_pred3d_mwd = [None]*5
min_x, max_x, min_y, max_y, min_z, max_z = [None]*6
eng_ass = None
eng_ass_east = 'centroid_x'
eng_ass_north = 'centroid_y'
eng_ass_elev = 'centroid_z'
avg_dim_x = 0
avg_dim_y = 0
avg_dim_z = 0

# Read to the button click to load all necessary info into global variables (naughty!)
# and then call each individual drawing function to plot everything
from dash.exceptions import PreventUpdate
from plotly.express import scatter, scatter_3d
from pyspark.sql.functions import col, min as spark_min, max as spark_max, avg as spark_avg, round as spark_round
from matplotlib.cm import get_cmap
cmap = get_cmap('RdYlGn_r')

# Draw graphs when the data source changes
def draw_eng_ass(boundary, btrace):
    global df_eng_ass
    global eng_ass    
    global eng_ass_east
    global eng_ass_north
    global min_x
    global max_x
    global min_y
    global max_y
    global min_z
    global max_z

    eng_cats = df_eng_ass.select(eng_ass).distinct().dropna().orderBy(eng_ass).rdd.flatMap(lambda x:x).collect()

    pdf = df_eng_ass\
        .where('%s between %f and %f and %s between %f and %f and %s between %f and %f'%(eng_ass_east, min_x-10, max_x+10, eng_ass_north, min_y-10, max_y+10, eng_ass_elev, min_z-10, max_z+10))\
        .groupBy(eng_ass_east, eng_ass_north).agg(spark_round(spark_avg(eng_ass)).name(eng_ass))\
        .withColumn(eng_ass, col(eng_ass).cast('int').cast('string'))\
        .select(eng_ass_east, eng_ass_north, eng_ass)\
        .dropna()\
        .toPandas()
    if len(pdf.index)==0:
        print("WARNING: couldn't find Block Model data for this area!")
        print('\tWanted: x:[%f,%f) y[%f,%f) z[%f,%f)'%(min_x, max_x, min_y, max_y, min_z, max_z))
        from pyspark.sql.functions import min as smin, max as smax
        ea_min_x, ea_max_x, ea_min_y, ea_max_y, ea_min_z, ea_max_z = df_eng_ass.select(eng_ass_east, eng_ass_north, eng_ass_elev)\
            .groupBy().agg(smin(eng_ass_east), smax(eng_ass_east), smin(eng_ass_north), smax(eng_ass_north), smin(eng_ass_elev), smax(eng_ass_elev) )\
            .first()
        print('\tAvailable: x[%f,%f) y[%f,%f) z[%f,%f)'%(ea_min_x, ea_max_x, ea_min_y, ea_max_y, ea_min_z, ea_max_z))
        return None
    fig = scatter(
        pdf, 
        x=eng_ass_east, y=eng_ass_north, color=eng_ass, 
        color_discrete_map={ '%d'%(c):'rgba(%f,%f,%f,%f)'%(cmap(c/len(eng_cats))) for c in eng_cats}, category_orders={eng_ass:['%d'%(c) for c in eng_cats]},
        width=500, height=500,
        render_mode='webgl'
    )\
    .update_traces(marker_size=4)\
    .update_xaxes(tickformat='f')\
    .update_yaxes(tickformat='f')\
    .update_layout(xaxis_title='Easting (m)', yaxis=dict(title='Northing (m)', scaleanchor='x', scaleratio=1), plot_bgcolor='rgba(0,0,0,0)', paper_bgcolor='rgba(0,0,0,0)')

    selected_eng = []
    for trace in fig.data:
        x, y = trace['x'], trace['y']
        selected_eng.append([])
        for i in range(len(x)):
            if within(x[i], y[i], boundary):
                selected_eng[len(selected_eng)-1].append(i)
    
    # fig.update_traces(selectedpoints=selected_eng)

    fig.add_trace(btrace)

    # selected_eng = []
    # for cat in eng_cats:
    #     tmp = [ idx for idx,r in pdf[pdf[eng_ass]=='%d'%(cat)].reset_index(drop=True).iterrows() if Point((r[eng_ass_east], r[eng_ass_north])).within(boundary) ]
    #     if len(tmp)>0:
    #         selected_eng.append(tmp)
    return fig, selected_eng

def draw_pred2d_geo(boundary, btrace, k):
    global df_pred2d_geo
    global eng_ass_east
    global eng_ass_north
    global min_x
    global max_x
    global min_y
    global max_y
    global avg_dim_x
    global avg_dim_y
    global avg_dim_z
    
    # orica_cats = df_pred2d_geo.select('Hardness').distinct().dropna().orderBy('Hardness').rdd.flatMap(lambda x:x).collect()
    orica_cats = [ x for x in range(1,k+1) ]

    pdf = df_pred2d_geo\
        .where('Easting between %f and %f and Northing between %f and %f'%(min_x, max_x, min_y, max_y))\
        .withColumn('Hardness', col('Hardness').cast('int').cast('string'))\
        .toPandas()
    fig = scatter(pdf, 
        x='Easting', y='Northing', color='Hardness',
        color_discrete_map={ '%d'%(c):'rgba(%f,%f,%f,%f)'%(cmap(c/len(orica_cats))) for c in orica_cats}, category_orders={'Hardness':['%df'%(c) for c in orica_cats]},
        width=500, height=500,
        render_mode='webgl'
    )\
    .update_traces(marker_size=4)\
    .update_xaxes(tickformat='f')\
    .update_yaxes(tickformat='f')\
    .update_layout(xaxis_title='Easting (m)', yaxis=dict(title='Northing (m)', scaleanchor='x', scaleratio=1), plot_bgcolor='rgba(0,0,0,0)', paper_bgcolor='rgba(0,0,0,0)')

    selected_geo = []
    for trace in fig.data:
        x, y = trace['x'], trace['y']
        selected_geo.append([])
        for i in range(len(x)):
            if within(x[i], y[i], boundary):
                selected_geo[len(selected_geo)-1].append(i)

    fig.add_trace(btrace)

    return fig, selected_geo

def draw_pred2d_mwd(buf, k):
    global df_pred2d_mwd
    
    # orica_cats = df_pred2d_mwd.select('Hardness').distinct().dropna().orderBy('Hardness').rdd.flatMap(lambda x:x).collect()
    orica_cats = [ x for x in range(1,k+1) ]
    
    fig = scatter(df_pred2d_mwd.withColumn('Hardness', col('Hardness').cast('integer').cast('string')).toPandas(), 
        x='CollarEasting', y='CollarNorthing', color='Hardness',
        color_discrete_map={ '%.0f'%(c):'rgba(%f,%f,%f,%f)'%(cmap(c/len(orica_cats))) for c in orica_cats}, category_orders={'Hardness':['%.0f'%(c) for c in orica_cats]},
        width=500, height=500,
        render_mode='webgl'
    )\
    .update_traces(marker_size=4)\
    .update_xaxes(tickformat='f')\
    .update_yaxes(tickformat='f')\
    .update_layout(xaxis_title='Easting (m)', yaxis=dict(title='Northing (m)', scaleanchor='x', scaleratio=1), plot_bgcolor='rgba(0,0,0,0)', paper_bgcolor='rgba(0,0,0,0)')

    boundary, bound_polys = get_boundary(df_pred2d_mwd, xcol='CollarEasting', ycol='CollarNorthing', buf=buf)

    selected_mwd = [ [ v for v in range(len(t['x'])) ] for t in fig['data'] ]

    return fig, selected_mwd, boundary, bound_polys

def draw_pred3d_mwd(k):
    global df_pred3d_mwd
    
    # orica_cats = df_pred3d_mwd.select('Hardness').distinct().dropna().orderBy('Hardness').rdd.flatMap(lambda x:x).collect()
    orica_cats = [ x for x in range(1,k+1) ]

    return scatter_3d(
        df_pred3d_mwd.distinct().withColumn('Hardness', col('Hardness').cast('integer').cast('string')).select('SampleEasting', 'SampleNorthing', 'SampleElevation', 'Hardness').toPandas(), 
        x='SampleEasting', y='SampleNorthing', z='SampleElevation', 
        color='Hardness', color_discrete_map={ '%.0f'%(c):'rgba(%f,%f,%f,%f)'%(cmap(c/len(orica_cats))) for c in orica_cats}, category_orders={'Hardness':['%.0f'%(c) for c in orica_cats]},
    ).update_traces(
        marker_size=1
    ).update_layout(
        showlegend=True,
        legend_bgcolor='rgba(0,0,0,0)',
        legend_x=0.3,
        legend_y=1,
        legend_orientation='h',
        plot_bgcolor='rgba(0,0,0,0)', paper_bgcolor='rgba(0,0,0,0)',
        scene = {
            'xaxis':{ 'title':'Easting (m)', 'showbackground':False },
            'yaxis':{ 'title':'Northing (m)', 'showbackground':False },
            'zaxis':{ 'title':'Elevation (m)', 'showbackground':False },
            'aspectmode':'data',
        },
        width=1400, height=700
    )

def plot_boundary(boundaries):
    # generate plotly polygons using their suggested method (https://plotly.com/python/shapes/)
    bxy = np.array([[None,None]])  
    for p in boundaries:
        bxy = np.append(bxy, np.array(p), axis=0)
        bxy = np.append(bxy, [[None,None]], axis=0)
    bxy = np.delete(bxy, 0, 0)
    bxy = np.delete(bxy, bxy.shape[0]-1, 0)
    # plot blast boundaries
    return Scattergl(name='Blast Boundary', x=bxy[:,0],y=bxy[:,1],mode='lines',line_color='blue',opacity=0)

@app.callback(
    [
        Output('clientside_viz_eng_ass', 'data'),
        Output('clientside_viz_pred2d_geo', 'data'),
        Output('clientside_viz_pred2d_mwd', 'data'),
        # Output('viz_eng_ass', 'figure'),
        # Output('viz_pred2d_geo', 'figure'),
        # Output('viz_pred2d_mwd', 'figure'),
        Output('clientside_3d_fig', 'data'),
        # Sliders parameters
        Output('sld_3d_x', 'min'),
        Output('sld_3d_x', 'max'),
        Output('sld_3d_x', 'value'),
        Output('sld_3d_y', 'min'),
        Output('sld_3d_y', 'max'),
        Output('sld_3d_y', 'value'),
        Output('sld_3d_z', 'min'),
        Output('sld_3d_z', 'max'),
        Output('sld_3d_z', 'value'),
        Output('2d_geo_lbl', 'src'),
        Output('2d_geo_mae', 'src'),
        Output('2d_geo_feat_imp', 'src'),
        Output('2d_mwd_lbl', 'src'),
        Output('2d_mwd_mae', 'src'),
        Output('2d_mwd_feat_imp', 'src'),
        Output('3d_mwd_mae', 'src'),
        Output('3d_mwd_feat_imp', 'src'),
        Output('2d_geo_metrics', 'data'),
        Output('2d_mwd_metrics', 'data'),
        Output('3d_mwd_metrics', 'data'),
        Output('geo_feats', 'children'),
        Output('masked_eng_ass', 'data'),
        Output('masked_geo', 'data'),
        Output('masked_mwd', 'data')
    ],
    [ 
        Input('btn_load', 'n_clicks'),
        Input('dilation_sz', 'value')
    ],
    [
        State('dd_eng_ass_data', 'value'),
        State('dd_eng_ass_col', 'value'),
        State('dd_pred2d_geo_data', 'value'),
        State('dd_pred2d_mwd_data', 'value'),
        State('dd_pred3d_mwd_data', 'value')
    ]
)
def update(n_clicks, dilation_sz, chosen_run_id_data, my_eng_ass, chosen_run_id_pred2d_geo, chosen_run_id_pred2d_mwd, chosen_run_id_pred3d_mwd):
    # Don't do anything if some of the data still wasn't chosen
    if chosen_run_id_data==None or my_eng_ass==None or chosen_run_id_pred2d_geo==None or chosen_run_id_pred2d_mwd==None or chosen_run_id_pred3d_mwd==None:
        raise PreventUpdate

    triggered = [ t['prop_id'].split('.')[0] for t in dash.callback_context.triggered ]

    session_id = str(uuid.uuid4())
    print('Loading data (session %s)...'%(session_id))

    # Set paths
    DATA_RUN_DIR = '%s/%s/artifacts/'%(data_experiment.artifact_location,chosen_run_id_data)
    PRED2D_GEO_RUN_DIR = '%s/%s/artifacts/'%(pred_experiment.artifact_location,chosen_run_id_pred2d_geo)
    PRED2D_MWD_RUN_DIR = '%s/%s/artifacts/'%(pred_experiment.artifact_location,chosen_run_id_pred2d_mwd)
    PRED3D_RUN_DIR = '%s/%s/artifacts/'%(pred_experiment.artifact_location,chosen_run_id_pred3d_mwd)

    # Load data - we're using global variables here, but that's naughty! Don't do that!
    global df_BlastHole
    global df_eng_ass
    global df_pred2d_geo
    global df_pred2d_mwd
    global df_pred3d_mwd

    global min_x
    global max_x
    global min_y
    global max_y
    global min_z
    global max_z
    global eng_ass
    global orica_cats
    global eng_cats
    global avg_dim_x
    global avg_dim_y
    global avg_dim_z

    df_BlastHole = spark.read.parquet(DATA_RUN_DIR+'Wrangled_BlastHole.parquet')
    df_eng_ass = spark.read.parquet(DATA_RUN_DIR+'BlockModel.parquet')
    df_pred2d_geo = spark.read.csv(PRED2D_GEO_RUN_DIR+'Clusters.csv', inferSchema=True, header=True)\
        .drop('_c0')\
        .withColumnRenamed('centroid_x', 'Easting')\
        .withColumnRenamed('centroid_y', 'Northing')\
        .withColumnRenamed('Prediction', 'Hardness')
    df_pred2d_mwd = spark.read.csv(PRED2D_MWD_RUN_DIR+'Clusters.csv', inferSchema=True, header=True)\
        .join(df_BlastHole.select('DrillPattern', 'HoleNumber', 'CollarEasting', 'CollarNorthing').distinct(), ['DrillPattern', 'HoleNumber'])\
        .select('DrillPattern', 'HoleNumber', 'CollarEasting', 'CollarNorthing', 'Hardness')
    df_pred3d_mwd = spark.read.csv(PRED3D_RUN_DIR+'Clusters.csv', inferSchema=True, header=True)\
        .hint('broadcast').hint('range_join', 10)\
        .join(df_BlastHole, ['DrillPattern', 'HoleNumber', 'SampleDepth'])\
        .select('DrillPattern', 'HoleNumber', 'SampleEasting', 'SampleNorthing', 'SampleElevation', col('PredictedHardness').alias('Hardness'))

    # Calculate the rest of the necessary info
    
    eng_ass = my_eng_ass
    
    min_x, max_x, min_y, max_y, min_z, max_z = df_pred3d_mwd.agg(spark_min('SampleEasting'), spark_max('SampleEasting'), spark_min('SampleNorthing'), spark_max('SampleNorthing'), spark_min('SampleElevation'), spark_max('SampleElevation')).rdd.flatMap(lambda x:x).collect()
    avg_dim_x, avg_dim_y, avg_dim_z = df_eng_ass.agg(spark_avg('dim_x'), spark_avg('dim_y'), spark_avg('dim_z')).rdd.flatMap(lambda x:x).collect()

    # Download the model metric artifacts and metrics
    session_dir = local_dir+'artifacts-'+session_id+'/'
    if not os.path.exists(session_dir):
        os.mkdir(session_dir)

    model_run_id_2d_geo = pred2d_geo_runs.loc[pred2d_geo_runs['run_id']==chosen_run_id_pred2d_geo]['params.Model Run ID'].iloc[0]
    mlflow_client.download_artifacts(model_run_id_2d_geo, 'MAE_by_group.html', session_dir)
    os.rename(session_dir+'MAE_by_group.html', session_dir+'2d_geo_mae.html')
    mlflow_client.download_artifacts(model_run_id_2d_geo, 'Feature Importances.html', session_dir)
    os.rename(session_dir+'Feature Importances.html', session_dir+'2d_geo_feat_imp.html')
    pred_2d_geo_model_metrics = [mlflow_client.get_run(model_run_id_2d_geo).data.metrics]
    mlflow_client.download_artifacts(model_run_id_2d_geo, 'features.txt', session_dir)
    os.rename(session_dir+'features.txt', session_dir+'geo_features.txt')
    f = open(session_dir+'geo_features.txt')
    geo_feats = f.read()
    f.close()
    label_run_id_2d_geo = mlflow_client.get_run(model_run_id_2d_geo).data.params['Labelling Run ID']
    mlflow_client.download_artifacts(label_run_id_2d_geo, 'mse_hists_by_domain.html', session_dir)
    os.rename(session_dir+'mse_hists_by_domain.html', session_dir+'2d_geo_lbl_hists.html')
    k_2d_geo = int(mlflow_client.get_run(label_run_id_2d_geo).data.params['K'])

    model_run_id_2d_mwd = pred2d_mwd_runs.loc[pred2d_mwd_runs['run_id']==chosen_run_id_pred2d_mwd]['params.Model Run ID'].iloc[0]
    mlflow_client.download_artifacts(model_run_id_2d_mwd, 'MAE_by_group.html', session_dir)
    os.rename(session_dir+'MAE_by_group.html', session_dir+'2d_mwd_mae.html')
    mlflow_client.download_artifacts(model_run_id_2d_mwd, 'Feature Importances.html', session_dir)
    os.rename(session_dir+'Feature Importances.html', session_dir+'2d_mwd_feat_imp.html')
    pred_2d_mwd_model_metrics = [mlflow_client.get_run(model_run_id_2d_mwd).data.metrics]
    label_run_id_2d_mwd = mlflow_client.get_run(model_run_id_2d_geo).data.params['Labelling Run ID']
    mlflow_client.download_artifacts(label_run_id_2d_mwd, 'mse_hists_by_domain.html', session_dir)
    os.rename(session_dir+'mse_hists_by_domain.html', session_dir+'2d_mwd_lbl_hists.html')
    k_2d_mwd = int(mlflow_client.get_run(label_run_id_2d_mwd).data.params['K'])

    model_run_id_3d_mwd = pred3d_runs.loc[pred3d_runs['run_id']==chosen_run_id_pred3d_mwd]['params.Model Run ID'].iloc[0]
    mlflow_client.download_artifacts(model_run_id_3d_mwd, 'MAE_by_group.html', session_dir)
    os.rename(session_dir+'MAE_by_group.html', session_dir+'3d_mwd_mae.html')
    mlflow_client.download_artifacts(model_run_id_3d_mwd, 'Feature Importances.html', session_dir)
    os.rename(session_dir+'Feature Importances.html', session_dir+'3d_mwd_feat_imp.html')
    pred_3d_mwd_model_metrics = [mlflow_client.get_run(model_run_id_3d_mwd).data.metrics]
    label_run_id_3d_mwd = mlflow_client.get_run(model_run_id_3d_mwd).data.params['Labelling Run ID']
    k_3d_mwd = int(mlflow_client.get_run(label_run_id_3d_mwd).data.params['K'])

    # Populate the cards with the plots
    mwd_fig, selected_mwd, boundary, bound_polys = draw_pred2d_mwd(dilation_sz, k_2d_mwd)
    boundary_trace = plot_boundary(boundary)
    mwd_fig.add_trace(boundary_trace)
    eng_ass_fig, selected_eng = draw_eng_ass(bound_polys, boundary_trace)
    geo_fig, selected_geo = draw_pred2d_geo(bound_polys, boundary_trace, k_2d_geo)
    
    return  (
        eng_ass_fig, geo_fig, mwd_fig, draw_pred3d_mwd(k_3d_mwd), # Figures
        min_x-10, max_x+10, [min_x-10, max_x+10], min_y-10, max_y+10, [min_y-10, max_y+10], min_z-2, max_z+2, [min_z-2, max_z+2], # Sliders bounds
        # Embedded pre-generated plots
        os.path.join(session_dir, '2d_geo_lbl_hists.html'), os.path.join(session_dir, '2d_geo_mae.html'), os.path.join(session_dir, '2d_geo_feat_imp.html'), 
        os.path.join(session_dir, '2d_mwd_lbl_hists.html'), os.path.join(session_dir, '2d_mwd_mae.html'), os.path.join(session_dir, '2d_mwd_feat_imp.html'), 
        os.path.join(session_dir, '3d_mwd_mae.html'), os.path.join(session_dir, '3d_mwd_feat_imp.html'),
        # Metrics Table Data
        pred_2d_geo_model_metrics, pred_2d_mwd_model_metrics, pred_3d_mwd_model_metrics,
        geo_feats,
        # Masked block model only with area with holes
        selected_eng,
        selected_geo,
        selected_mwd
    )

app.clientside_callback(
    ClientsideFunction(namespace='clientside', function_name='clientside_function_wrapper_2d'),
    [
        Output('viz_eng_ass', 'figure'),
        Output('viz_pred2d_geo', 'figure'),
        Output('viz_pred2d_mwd', 'figure'),
    ],
    # The following need to include all inputs and data that may be required by any of the callbacks in the wrapper. Every time.
    [  
        Input('viz_eng_ass', 'relayoutData'),
        Input('viz_pred2d_geo', 'relayoutData'),
        Input('viz_pred2d_mwd', 'relayoutData'),
        Input('2d_pt_sz', 'value'),
        Input('chk_mask', 'value'),
        Input('clientside_viz_eng_ass', 'data'),
        Input('clientside_viz_pred2d_geo', 'data'),
        Input('clientside_viz_pred2d_mwd', 'data'),
    ],[
        State('masked_eng_ass', 'data'),
        State('masked_geo', 'data'),
        State('masked_mwd', 'data')
    ]
)

# For slicing the 3d view
app.clientside_callback(
    ClientsideFunction(namespace='clientside', function_name='slice_3d'),
    Output('viz_pred3d_mwd', 'figure'),
    [  
        Input('clientside_3d_fig', 'data'), 
        Input('sld_3d_x', 'value'),
        Input('sld_3d_y', 'value'),
        Input('sld_3d_z', 'value') 
    ],
)

# Remove all downloaded files from 'assets/'
import atexit
import glob
from shutil import rmtree
def cleanup():
    os.chdir('assets')
    for f in glob.glob('artifacts-*'):
        rmtree(f)
    os.chdir('..')
atexit.register(cleanup)

if __name__ == '__main__':
    app.run_server(debug=False, dev_tools_hot_reload=False, use_reloader=False)
