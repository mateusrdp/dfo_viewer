from alphashape import alphashape, optimizealpha
from shapely.geometry import Polygon, MultiPolygon, MultiPoint, Point

def get_bbox(df, xcol='CollarEasting', ycol='CollarNorthing'):
  df = df.repartition(getNumCores(), xcol)
  min_e, max_e, min_n, max_n = df.agg(spark_min(xcol), spark_max(xcol), spark_min(ycol), spark_max(ycol)).rdd.flatMap(lambda x:x).collect()
  min_e -= buf
  max_e += buf
  min_n -= buf
  max_n += buf
  vertices = [(min_e, min_n), (min_e, max_n), (max_e, max_n), (max_e, min_n), (min_e, min_n)]
  return vertices, Polygon(vertices)

def get_boundary(df, xcol='CollarEasting', ycol='CollarNorthing', buf=5):
  points = [ (r[xcol],r[ycol]) for r in df.select(xcol, ycol).distinct().collect() ] 
  polys = MultiPoint(points).buffer(buf)
  # This can be either:
  # shapely.geometry.polygon.Polygon
  # shapely.geometry.multipolygon.MultiPolygon
  if type(polys)==MultiPolygon:
    boundaries = [list(p.exterior.coords) for p in polys] # There's a list of polygons, iterate through them
    return boundaries, polys
  elif type(polys)==Polygon:
    boundaries = [ list(polys.exterior.coords) ] # There's only one poly - can't iterate
    return boundaries, [polys]
  # If it got here, something funky happened and the types are not as expected
  raise Exception("ERROR: get_boundary(): the obtained boundary is not a Polygon or MultiPolygon (it's %s)!"%(type(polys)))
  return None, None

__flatten = lambda list_of_lists: [ item for sublist in list_of_lists for item in sublist ]

def get_concave_hull(boundary, optimise=False, buf=5):
  points = __flatten(boundary)
  alpha = 0.
  if optimise:
    alpha = optimizealpha(points)
  concave_hull = alphashape(points, alpha)
  concave_hull_vertices = list(Polygon(concave_hull).buffer(float(buf)).exterior.coords)
  return concave_hull_vertices, Polygon(concave_hull_vertices)

def within(x,y,area): 
  if type(area)==MultiPolygon:
    return Point(x,y).within(area)
  elif isinstance(area, list): # Because get_boundary() returns a list when it's a Polygon
    for a in area:
      if type(a)!=Polygon and type(a)!=MultiPolygon:
        raise Exception("ERROR: within(): the area is a list, but one or more elements are not a Polygon or MultiPolygon! (this one was %s)"%(type(a)))
      if not Point(x,y).within(area[0]):     
        return False
      return True
  raise Exception("ERROR: within(): the area is not a Polygon or Multipolygon (or list of those) (it's %s)"%(type(area)))
  