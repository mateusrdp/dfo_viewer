from azure.keyvault.secrets import SecretClient
# NOTE: this requires the following environment variables to be set appropriately: AZURE_CLIENT_ID, AZURE_CLIENT_SECRET, AZURE_TENANT_ID
#       this assumes that the azure vault has a service principal (App) with at least Key Vault Secrets user role assignments,
#       and that the above are its client id, client secret and tenant id
from azure.identity  import DefaultAzureCredential
# Shut azure up
import logging
logger = logging.getLogger('azure')
logger.setLevel(logging.CRITICAL)

credential = DefaultAzureCredential()
import os
vault_client = SecretClient(vault_url=f"https://oricadatabrickskv.vault.azure.net", credential=credential)