from pyspark.sql import SparkSession

print("... Initializing SparkSession ...")

# clusterId 0219-235345-shot16 = Mat's Viz Cluster
# Configure databricks-connect at runtime, to avoid having the token in plain text in the .databricks-connect file
# The token should be gotten from a Key Vault
# Everything else can be hardcoded because they're not sensitive information
from vault import vault_client
dbx_token = vault_client.get_secret('databricks-token')

# This is for Databricks CLI, required by Mlflow
import os
os.environ['DATABRICKS_HOST']='https://adb-5736147455068205.5.azuredatabricks.net'
os.environ['DATABRICKS_TOKEN']=dbx_token.value
# This is for databricks-connect
spark = SparkSession.builder\
    .appName("Design for Outcome - Prediction Viewer (Spence)")\
    .config('spark.databricks.service.address', 'https://adb-5736147455068205.5.azuredatabricks.net')\
    .config('spark.databricks.service.token', dbx_token.value)\
    .config('spark.databricks.service.clusterId', '0219-235345-shot16')\
    .config('spark.databricks.service.orgId', '5736147455068205')\
    .config('spark.databricks.service.port', '15001')\
    .getOrCreate()

sc = spark.sparkContext

# Setup mlflow
import mlflow
from mlflow.tracking import MlflowClient
mlflow.set_tracking_uri('databricks') # This needs one to run '$ databricks configure --token' first
mlflow_client = MlflowClient()